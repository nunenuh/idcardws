from classify.predictor import MobileNetClassifyOnnx

base_path = '/app/weights'


classify_config = {
    'weight': f'{base_path}/classify/classify_mobilenetv2.onnx',
    'idx2class': f'{base_path}/classify/idcard_classname.json'
}

classify_predictor = MobileNetClassifyOnnx(**classify_config)
