from typing import Optional, List

from pydantic import BaseModel, EmailStr

class PredictionData(BaseModel):
    provinsi: str
    kabupaten: str
    nik: str
    ttl: str
    tempat_lahir: str
    tanggal_lahir: str
    gender: str
    goldar: str
    alamat: str
    rtrw: str
    rt: str
    rw: str
    kelurahan: str
    kecamatan: str
    agama: str
    perkawinan: str
    pekerjaan: str
    kewarganegaraan: str
    berlaku: str
    sign_place: str
    sign_date: str


class PredictionTime(BaseModel):
    unet: str
    deskew: str
    craft: str
    crnn: str
    layoutlm: str    
    
    
class Prediction(BaseModel):
    data: PredictionData
    time: PredictionTime

class Classify(BaseModel):
    clazz: str
    confidence: str
    
    
class FaceVerify(BaseModel):
    detected: bool
    verified: bool
    distance: Optional[float]
    max_threshold_to_verify: Optional[float]
    model: Optional[str]
    similarity_metric: Optional[str]
    faces: Optional[List[str]]
    image: Optional[str]
    
# {'provinsi': 'PROVINSI JAWA BARAT',
#  'kabupaten': 'KOTA BEKASI',
#  'nik': '3275065109900006',
#  'nama': 'NURHAYATI',
#  'ttl': 'BEKASI, 11-09-1990',
#  'tempat_lahir': 'BEKASI',
#  'tanggal_lahir': '11-09-1990',
#  'gender': 'PEREMPUAN',
#  'goldar': '',
#  'alamat': 'KP BUARAN 001',
#  'rtrw': '003',
#  'rt': '',
#  'rw': '',
#  'kelurahan': 'HARAPAN MGI YA',
#  'kecamatan': 'MEDAN SATRIA',
#  'agama': 'ISLAM',
#  'perkawinan': 'BELUM KAWIN',
#  'pekerjaan': 'PELAJAR/MAHASISWA',
#  'kewarganegaraan': 'WNI',
#  'berlaku': '11-09-2017',
#  'sign_place': 'KOTA BEKASI',
#  'sign_date': '27-06-2012'}