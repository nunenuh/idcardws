from typing import Optional, List

from pydantic import BaseModel, EmailStr

class FaceDetect(BaseModel):
    found: bool
    count: int
    link: Optional[List[str]]
    
class FaceVerify(BaseModel):
    detected: bool
    verified: bool
    distance: Optional[float]
    max_threshold_to_verify: Optional[float]
    model: Optional[str]
    similarity_metric: Optional[str]
    faces: Optional[List[str]]
    image: Optional[str]

class Classify(BaseModel):
    clazz: str
    confidence: str
    
