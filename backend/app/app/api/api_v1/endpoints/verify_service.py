from typing import Any, List

from fastapi import APIRouter, Body, Depends, HTTPException, Request
from fastapi.encoders import jsonable_encoder
from pydantic.networks import EmailStr
from sqlalchemy.orm import Session

from app import crud, models, schemas
from app.api import deps
from app.core.config import settings
from app.utils import send_new_account_email
from fastapi import FastAPI, File, UploadFile
from app.libml.predictor_wrapper import classify_predictor
from app.libml import face_verify, utils



router = APIRouter()

@router.post('/face_detection', response_model=schemas.FaceDetect)
async def detect(
    request: Request,
    file: UploadFile = File(...),
    current_user: models.User = Depends(deps.get_current_active_superuser),
)->Any:
    if file.content_type == "image/jpeg" or file.content_type=="image/png":
        file_contents = await file.read()
        img = utils.convert_buffer_to_image(file_contents, file)
        result = face_verify.detect_faces(img, request)
        return schemas.FaceDetect(**result)


@router.post('/idcard_detection', response_model=schemas.Classify)
async def classify(
    file: UploadFile = File(...),
    current_user: models.User = Depends(deps.get_current_active_superuser),
)->Any:
     if file.content_type == "image/jpeg" or file.content_type=="image/png":
        file_contents = await file.read()
        image = utils.convert_buffer_to_image(file_contents, file)
        image = utils.convert_npimage_to_pilimage(image, file)
        result = classify_predictor.predict(image)
        return schemas.Classify(clazz=result['class'], confidence=result['confidence'])
    
    
@router.post('/idcard_face_verify')
async def verify(
    request: Request,
    file: UploadFile = File(...),
    current_user: models.User = Depends(deps.get_current_active_superuser),
)->Any:
    if file.content_type == "image/jpeg" or file.content_type=="image/png":
        file_contents = await file.read()
        img = utils.convert_buffer_to_image(file_contents, file)
        result = face_verify.idcard_face_verify(img, request)
        return schemas.FaceVerify(**result)
